﻿Param (
	#driver file path
	[Parameter(Mandatory=$true,ValueFromPipeLine=$true)]
	[string]
	$ExtractFrom,
	#path to extract to
	[Parameter(Mandatory=$true,ValueFromPipeLine=$true)]
	[string]
	$ExtractTo
)
$d = New-Object System.IO.DirectoryInfo($ExtractFrom)
$files = $d.GetFiles("sp*.exe")
foreach ($f in $files)
{
	$path = $ExtractTo + "\" + $f.BaseName
	$cmdline = $f.FullName
	$params = "-e -f " + $path + " -s"
	Write-Host "Creating $path..."
	New-Item $path -ItemType directory -Force | Out-Null
	Write-Host "Extracting drivers from $ExtractFrom" $f.BaseName "to $path"
	cmd /c $cmdline $params
}
Write-Host "Done"