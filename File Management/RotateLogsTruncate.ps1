<#
Rotates logs by copying the file and truncating the original
Enumerates the list of servers from Active Directory ($SearchRoot)
Requires: Quest
Updated: 2016.07.08
#>

Param ( 
	[Parameter(Mandatory=$True)]
    	[string]$TargetFolder,
    [Parameter(Mandatory=$True)]
		[array]$LogFile,
	[Parameter(Mandatory=$True)]
		[array]$SearchRoot,
    [Parameter(Mandatory=$False)]
		[boolean]$Recurse = $True,
	[Parameter(Mandatory=$False)]
		[string]$MailTo = "alerts@example.com",
    [Parameter(Mandatory=$False)]
		[string]$MailFrom = "no-reply@example.com",
	[Parameter(Mandatory=$False)]
		[string]$MailServer = "mail.example.com"
)

function StampTime() {
	return "[" + (Get-Date -Format u) + "]"
}

$scriptName = ($MyInvocation.MyCommand).Name
$scriptPath = $MyInvocation.MyCommand.Path
$scriptDir = Split-Path $scriptPath
New-EventLog -LogName "Application" -Source "CustomScripts" -ErrorAction SilentlyContinue
Write-EventLog -LogName Application -Source "CustomScripts" -EventId 1000 -EntryType Information -Message "$scriptName Started"
try {
	# load up Quest
	if (! (Get-PSSnapin Quest.ActiveRoles.ADManagement -ErrorAction:SilentlyContinue) ) {
		Write-Host "Loading Quest Tools...`n"
	    Add-PSSnapin Quest.ActiveRoles.ADManagement
	}

	# if there's no trailing slash add it
	if ( !$TargetFolder.EndsWith("\") ) {
		$TargetFolder += "\"
	}
	# date to append
	$date = Get-Date -Format "yyyy-MM-dd-hhmmss"
	
	# list of server
	$Servers = Get-QADComputer -LdapFilter "(!(userAccountControl:1.2.840.113556.1.4.803:=2))" -SearchRoot $SearchRoot
	
	if ( $Servers.Count -gt 0 ) {
	# loop through the servers
		foreach ( $s in $Servers ) {
			# log
			$log = ""
			# build UNC path
			$UNCPath = "\\" + $s.Name + "\" + $TargetFolder
			# check the path
			if ( (Test-Path -Path $UNCPath) -eq $false ) {
				# path is bad
				$log = (StampTime) + (" $UNCPath appears to be unreachable or invalid`n")
			} else {
				# find log files
				$log += (StampTime) + (" Searching $UNCPath...`n")
				$LogFiles = $null
				if ( $Recurse -eq $True ) {
					$LogFiles = Get-Childitem -Path $UNCPath -Include $LogFile -Recurse
				} else {
					$LogFiles = Get-Childitem -Path $UNCPath -Include $LogFile
				}
				# do we have any?
				if ( $LogFiles -eq $null ) {
					# none
					$log += (StampTime) + ("Found 0 log files`n")
				} else {
					# got some
					if ( $LogFiles.Count -eq $null ) {
						$log += (StampTime) + ("Found 1 log file`n")
					} else {
						$log += (StampTime) + (" Found " + $LogFiles.Count + " log files`n")
					}
					# loop through the logs
					foreach ( $f in $LogFiles ) {
						if ( $f -ne $null ) {
							# copy & truncate the log file
							$newFile = $f.BaseName + "-" + $date + $f.Extension
							$newPath = $f.DirectoryName + "\" + $newFile
							$log += (StampTime) + ("Rotating " + $f.Name + " to $newFile`n")
							Copy-Item -Path $f.FullName -Destination $newPath -Force | Out-Null
							if ( Test-Path -Path $newPath ) {
								# truncate file
								$log += (StampTime) + ("Copy OK`n")
								Clear-Content -Path $f.FullName -Force
							} else {
								#copy failed
								$log +=  (StampTime) + ("Copy FAILED`n")
							}
						} else {
							
						};
						$log += (StampTime) + ("Complete")
					}
					# done with this server, on to the next
					Write-Host $log
					$MailSubject = "[SUCCESS] Rotate Logs - " + $s.Name + " (" + $LogFile + ")"
					Send-MailMessage -To $MailTo -From $MailFrom -Subject $MailSubject -Body $log -SmtpServer $MailServer -Priority Low
				}
			}
			Write-EventLog -LogName "Application" -Source "CustomScripts" -EventId 1000 -EntryType Information -Message $log
		}
	} else {
		# no servers
		$log = (StampTime) + (" No servers were returned from the query")
		Write-Host $log
		$MailSubject = "[ERROR] Rotate Logs - No servers"
		Send-MailMessage -To $MailTo -From $MailFrom -Subject $MailSubject -Body $log -SmtpServer $MailServer -Priority High
		Write-EventLog -LogName "Application" -Source "CustomScripts" -EventId 1000 -EntryType Error -Message $log
	}
} catch [System.Exception] {
	Write-Host (StampTime) "[ERROR] "$Error[0] -ForegroundColor Red
	$MailSubject = "[ERROR] Rotate Logs"
	Send-MailMessage -To $MailTo -From $MailFrom -Subject $MailSubject -Body $Error[0] -SmtpServer $MailServer -Priority High
	Write-EventLog -LogName Application -Source "CustomScripts" -EventId 1000 -EntryType Error -Message $Error[0]
}