<#
Rotates logs by copying the file and truncating the original
Updated: 2014.11.14
#>

Param ( 
	[Parameter(Mandatory=$True)]
    	[string]$TargetFolder,
    [Parameter(Mandatory=$True)]
		[array]$LogFile,
    [Parameter(Mandatory=$False)]
		[boolean]$Recurse = $True,
	[Parameter(Mandatory=$false)]
		[Boolean]$SendEmail = $false,
	[Parameter(Mandatory=$False)]
		[string]$MailTo = "none@example.com",
    [Parameter(Mandatory=$False)]
		[string]$MailFrom = "none@example.com",
	[Parameter(Mandatory=$False)]
		[string]$MailServer = "mail.example.com"
)

# generates a nicely formatted time stamp for logging
function StampTime() {
	return "[" + (Get-Date -Format u) + "]"
}

# record the script name
$scriptName = ($MyInvocation.MyCommand).Name
# record the path
$scriptPath = $MyInvocation.MyCommand.Path
$scriptDir = Split-Path $scriptPath
# set the event source
New-EventLog -LogName "Application" -Source "CustomScripts" -ErrorAction SilentlyContinue
Write-EventLog -LogName Application -Source "CustomScripts" -EventId 1000 -EntryType Information -Message "$scriptName Started"

try {
	# if there's no trailing slash add it
	if ( !$TargetFolder.EndsWith("\") ) {
		$TargetFolder += "\"
	}
	
	# date to append
	$date = Get-Date -Format "yyyy-MM-dd-hhmmss"

	# log
	$log = ""
	# check the path
	if ( (Test-Path -Path $TargetFolder) -eq $false ) {
		# path is bad
		$log = (StampTime) + (" $TargetFolder appears to be unreachable or invalid`n")
	} else {
		# find log files
		$log += (StampTime) + (" Searching $LogPath...`n")
		$LogFiles = $null
		if ( $Recurse -eq $True ) {
			$LogFiles = Get-Childitem -Path $TargetFolder -Include $LogFile -Recurse
		} else {
			$LogFiles = Get-Childitem -Path $TargetFolder -Include $LogFile
		}
		# do we have any?
		if ( $LogFiles -eq $null ) {
			# none
			$log += (StampTime) + ("Found 0 log files`n")
		} else {
			# got some
			if ( $LogFiles.Count -eq $null ) {
				$log += (StampTime) + ("Found 1 log file`n")
			} else {
				$log += (StampTime) + (" Found " + $LogFiles.Count + " log files`n")
			}
			# loop through the logs
			foreach ( $f in $LogFiles ) {
				if ( $f -ne $null ) {
					# copy & truncate the log file
					$newFile = $f.BaseName + "-" + $date + $f.Extension
					$newPath = $f.DirectoryName + "\" + $newFile
					$log += (StampTime) + (" Rotating " + $f.Name + " to $newFile`n")
					Copy-Item -Path $f.FullName -Destination $newPath -Force | Out-Null
					if ( Test-Path -Path $newPath ) {
						# copy OK, truncate file
						$log += (StampTime) + (" Copy OK`n")
						Clear-Content -Path $f.FullName -Force
					} else {
						# copy failed
						$log +=  (StampTime) + ("Copy FAILED`n")
					}
				} 
				$log += (StampTime) + (" Complete")
			}
			# done output details to the console
			Write-Host $log
			if ( $SendEmail -eq $true ) {
				$MailSubject = "[SUCCESS] Rotate Logs - " + $s.Name + " (" + $LogFile + ")"
				Send-MailMessage -To $MailTo -From $MailFrom -Subject $MailSubject -Body $log -SmtpServer $MailServer -Priority Low
			}
		}
	}
	# write the details to the event log
	Write-EventLog -LogName "Application" -Source "CustomScripts" -EventId 1000 -EntryType Information -Message $log
} catch [System.Exception] {
	# something went wrong
	Write-Host (StampTime) "[ERROR] "$Error[0] -ForegroundColor Red
	if ( $SendEmail -eq $true ) {
		$MailSubject = "[ERROR] Rotate Logs"
		Send-MailMessage -To $MailTo -From $MailFrom -Subject $MailSubject -Body $Error[0] -SmtpServer $MailServer -Priority High
	}
	Write-EventLog -LogName Application -Source "CustomScripts" -EventId 1000 -EntryType Error -Message $Error[0]
}