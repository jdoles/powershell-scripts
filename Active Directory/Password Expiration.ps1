﻿function StampTime() {
	return "[" + (Get-Date -Format u) + "]"
}

#days until expiration
$notice = 10
#from address
$MailFrom = "no-reply@example.com"
#mail server
$MailServer = "mail.example.com"
# mail subject
$MailSubject = "Domain Password Expiration Notice"
# mail to (for summary)
$MailTo = "admins@example.com"
# help desk email
$MailHelpDesk = "helpdesk@example.com"

$scriptName = ($MyInvocation.MyCommand).Name
$scriptPath = $MyInvocation.MyCommand.Path
$scriptDir = Split-Path $scriptPath
New-EventLog -LogName "Application" -Source "CustomScripts" -ErrorAction SilentlyContinue
Write-EventLog -LogName Application -Source "CustomScripts" -EventId 1000 -EntryType Information -Message "$scriptName Started"
try {
	# load up Quest
	if (! (Get-PSSnapin Quest.ActiveRoles.ADManagement -ErrorAction:SilentlyContinue) ) {
		Write-Host "Loading Quest Tools...`n"
	    Add-PSSnapin Quest.ActiveRoles.ADManagement
	}

	#get the max/min age from AD
	$maxage = (Get-QADObject -Identity (Get-QADRootDSE).defaultNamingContextDN).MaximumPasswordAge.Days

	if ( $maxage -le 0 ) {
		Write-EventLog -LogName Application -Source "CustomScripts" -EventId 1000 -EntryType Information -Message "$scriptName`nMaximum Password Age not configured...exiting"
	} else {
		#calculated expression for expiration
		$expires = @{n="expires";e={$maxage - $_.passwordAge.Days}}

		#list of users
		$users = Get-QADUser -Enabled -PasswordNeverExpires:$false -Email * -SizeLimit 0 | Select-Object Name, Email, @{n="Expires";e={$maxage - $_.passwordAge.Days}} | Where-Object {$_.Expires -le $notice}

		[string]$logentry = ""
		$logentry = "$scriptName`nUsers with passwords nearing expiration: " + $users.Count + "`n"

		if ( $users.Count -gt 0 ) {
			$client = New-Object System.Net.Mail.SmtpClient($MailServer)
			$users | foreach {
				$logentry += $_.Name + "`t" + $_.expires + " days"
				
				$message = "<span style=`"font-family: 'Verdana','sans-serif'`"><span style=`"font-size: 14pt; font-weight: bold`">$MailSubject</span><br><br>"
				$message += $_.Name + ",<br>"
				if ( $_.expires -gt 0 ) {
					# still have some time left
					$message += "Your password will expire in <span style=`"color:red; font-weight: bold; text-decoration: underline`">" + $_.expires + "</span> day(s).  Please consider changing your password before it expires.<br><br>"
				} 
				if ( $_.expires -le 0 ) {
					# password has expired
					$message += "Your password <span style=`"color:red; font-weight: bold; text-decoration: underline`">has expired</span>.  Please consider changing your password.<br><br>"
				}
				$message += "You can change your password at any time by logging into your workstation and pressing CTRL+ALT+DEL.  Then choose `"Change a password...`" from the options.<br><br>"
				$message += "If you have any questions, please contact the Help Desk - <a href=`"mailto:$MailHelpDesk?subject=Password Expiration Notice`">$MailHelpDesk</a><br><br>"
				$message += "Notice Generated: " + (Get-Date -Format g) + "</span>"
				
				try {
					#$client.Send($message)
					Send-MailMessage -Priority Low -BodyAsHtml -SmtpServer $MailServer -From $MailFrom -To $_.Email -Subject $MailSubject -Body $message
					$logentry += "`t[SUCCESS]`n"
				} catch [System.Net.Mail.SmtpException] {
					$logentry += "`t[ERROR " + $_ + "]`n"
				}
				# sleep to help prevent tarpit issues
				Start-Sleep 6
			}
			Write-EventLog -LogName Application -Source "CustomScripts" -EventId 1000 -EntryType Information -Message $logentry
			
			$message = "<span style=`"font-family: 'Verdana','sans-serif'`"><span style=`"font-size: 14pt; font-weight: bold`">Domain Password Expiration Notice Summary</span><br><br>"
			$message += $logentry.Replace("`n", "<br>")
			Send-MailMessage -Priority Low -BodyAsHtml -SmtpServer $MailServer -From $MailFrom -To $MailTo -Subject "$MailSubject Summary" -Body $message
		} else {
			Write-EventLog -LogName Application -Source "CustomScripts" -EventId 1000 -EntryType Information -Message "No users with expiring passwords found."
		}
	}
} catch [System.Exception] {
	Write-Host (StampTime) "[ERROR] "$Error[0] -ForegroundColor Red
	$MailSubject = "[ERROR] Doamin Password Expiration"
	Send-MailMessage -To $MailTo -From $MailFrom -Subject $MailSubject -Body $Error[0] -SmtpServer $MailServer -Priority High
	Write-EventLog -LogName Application -Source "CustomScripts" -EventId 1000 -EntryType Error -Message $Error[0]
}